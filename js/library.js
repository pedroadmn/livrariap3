﻿var myapp = angular.module('myapp', []);

myapp.controller('livrariaController', function ($scope) {

	var countId = 10;

    $scope.listLivros= [
        { id: 1, titulo: 'Como eu era antes de você', listaAutores: 'Jojo Moyes', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/como-eu-era-antes-de-voce.jpg', preco: 10, comentarios: [] },
        { id: 2, titulo: 'Depois de você', listaAutores: 'Jojo Moyes', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/depois-de-voce.jpg', preco: 12, comentarios: [] },
        { id: 3, titulo: 'O pequeno principe', listaAutores: 'Antoine de Saint-Exupéry', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/o-pequeno-principe.jpg', preco: 15, comentarios: [] },
        { id: 4, titulo: 'A Sereia', listaAutores: 'Keira Crass', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/a-sereia.jpg', preco: 21, comentarios: [] },
        { id: 5, titulo: 'Espada de Vidro', listaAutores: 'Victoria Aveyard', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/espada-de-vidro.jpg', preco: 24, comentarios: [] },
        { id: 6, titulo: 'Dois Mundos, Um Herói', listaAutores: 'Rezendeevil', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/dois-mundos-um-heroi.jpg', preco: 17, comentarios: [] },
        { id: 7, titulo: 'Grey', listaAutores: 'E.L. James', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/grey.png', preco: 13, comentarios: [] },
        { id: 8, titulo: 'Diário de Um Zumbi', listaAutores: 'Herobrine Books', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/diario-de-um-zumbi-do-minecraft.jpg', preco: 22, comentarios: [] },
        { id: 9, titulo: 'A Seleção', listaAutores: 'Kiera Cass', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/a-selecao.jpg', preco: 15, comentarios: [] },
        { id: 10, titulo: 'A Herdeira', listaAutores: 'Kiera Cass', descricao: 'Livro romantico que aborda o amor proibido entre dois jovens', url: 'http://veja.abril.com.br/svc/livros-mais-vendidos/img/ficcao/a-herdeira.jpg', preco: 19, comentarios: [] }
    ];
	
	$scope.add = function () {
		
        $scope.listLivros.push({
            id:++countId, titulo:$scope.titulo, listaAutores:$scope.listaAutores, descricao:$scope.descricao, url:$scope.url, preco:$scope.preco, comentarios : []
        });
            $scope.id = '';
            $scope.titulo = '';
            $scope.listaAutores = '';
            $scope.descricao = '';
            $scope.url = '';
            $scope.preco = '';
    }
	
    $scope.edit = function () {
        var index = getSelectedIndex($scope.id);
        $scope.listLivros[index].id = $scope.id;
        $scope.listLivros[index].titulo = $scope.titulo;
        $scope.listLivros[index].listaAutores = $scope.listaAutores;
        $scope.listLivros[index].descricao = $scope.descricao;
        $scope.listLivros[index].url = $scope.url;
        $scope.listLivros[index].preco = $scope.preco;
    }
		
    $scope.selectEdit = function(id){
        var index = getSelectedIndex(id);
        var livro = $scope.listLivros[index];
        $scope.id = livro.id;
        $scope.titulo = livro.titulo;
        $scope.listaAutores = livro.listaAutores;
        $scope.descricao = livro.descricao;
        $scope.url = livro.url;
        $scope.preco = livro.preco;
    }

    $scope.selectComment = function (id) {
        var index = getSelectedIndex(id);
        var livro = $scope.listLivros[index];
        $scope.titulo = livro.titulo;
        $scope.comentarios = livro.comentarios;
    }
	
    $scope.del = function(id){
        var result = confirm('Tem certeza que deseja deletar esse livro?');
        if(result==true){
            var index = getSelectedIndex(id);
            $scope.listLivros.splice(index, 1);	
        }
    }
	
    function getSelectedIndex(id){
        for(var i=0; i<$scope.listLivros.length; i++)
            if($scope.listLivros[i].id == id)
                return i;
        return -1;
    }
	
});
	
