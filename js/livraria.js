/**
 * Created by pedroadmn on 5/3/2016.
 */

angular.module('myapp', [])
    .controller('livrariaController', function ($http) {
        var livrariaController = this;
        livrariaController.bookList = [];

        // livrariaController.commentEnabled = false;
        // livrariaController.editData = {};
        //
        // for (var i=0, lenght = livrariaController.bookList.lenght; i < lenght; i++){
        //     livrariaController.editData[i] = false;
        // }

        //---$http
        // /api/book
        livrariaController.get_books = function () {
            function successCallback(response) {
                livrariaController.bookList = response.data;
            };

            function errorCallback() {
                console.log("Error");
            };

            $http.get('/api/book').then(successCallback, errorCallback);
        }

        livrariaController.get_books();

        livrariaController.add = function(){
            function successCallback(response) {
                livrariaController.bookList.push(response.data);
            };

            function errorCallback(response) {
                console.log(json);
                console.log(response);
            };

            var json = {
                id: '',
                title: livrariaController.title,
                author: livrariaController.author,
                description: livrariaController.description,
                cover: livrariaController.cover,
                price: livrariaController.price,
                comments: []
            };

            cleanField();

            $http.post('/api/book', json).then(successCallback, errorCallback);
        }

        livrariaController.remove = function(bookIndex){
            function successCallback(response) {
                livrariaController.bookList.splice(bookIndex, 1);
            };
            function errorCallback(response) {

            };

            $http.delete('/api/book/'+ livrariaController.bookList[bookIndex].id).then(successCallback, errorCallback);
        }

        livrariaController.selectEdit = function (bookIndex) {
            livrariaController.id = livrariaController.bookList[bookIndex].id;
            livrariaController.title = livrariaController.bookList[bookIndex].title;
            livrariaController.author = livrariaController.bookList[bookIndex].author;
            livrariaController.description = livrariaController.bookList[bookIndex].description;
            livrariaController.cover = livrariaController.bookList[bookIndex].cover;
            livrariaController.price = livrariaController.bookList[bookIndex].price;
        }

        function getSelectedIndex(id){
            for(var i=0; i<livrariaController.bookList.length; i++)
                if(livrariaController.bookList[i].id == id)
                    return i;
            return -1;
        }

        function cleanField(){
            livrariaController.id = '';
            livrariaController.title = '';
            livrariaController.author = '';
            livrariaController.description = '';
            livrariaController.cover = '';
            livrariaController.price = '';
        }

        livrariaController.update = function(){
            function successCallback(response) {
                console.log("Sucesso update");
            };
            function errorCallback(response) {
                console.log("Error  yhjhgrror");
                console.log(response);
            };

            var json = {
                id: livrariaController.id,
                title: livrariaController.title,
                author: livrariaController.author,
                description: livrariaController.description,
                cover: livrariaController.cover,
                price: livrariaController.price
            };

            cleanField();

            console.log(json);
            var bookIndex = getSelectedIndex(livrariaController.id);
            console.log(bookIndex);
            $http.put('/api/book/'+ livrariaController.bookList[bookIndex].id, json).then(successCallback, errorCallback);
        }



        livrariaController.comment = function(bookIndex, comment){
            livrariaController.bookList[bookIndex].comments.push(comment);
            function successCallback() {

            };
            function errorCallback() {

            };

            $http.put('/api/book/'+livrariaController.bookList[bookIndex].id+'/comment', json).then(successCallback, errorCallback);
        }

        livrariaController.edit = function(x){
            alert(x.id);
        }

    });